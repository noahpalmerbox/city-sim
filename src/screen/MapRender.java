package screen;

import java.awt.Graphics;

import resmgr.ImageLoader;

import main.Main;
import map.MapTile;

public class MapRender {
	Graphics g;
	public int x = 0;
	public int y = 0;

	public void init() {
		Main.render.map = ImageLoader.EmptyMap;
		g = Main.render.map.getGraphics();
	}

	public void update() {
		if (Main.frames % 1000 == 0) {
			for (int y = 0; y < 512; y++) {
				for (int x = 0; x < 512; x++) {
					g.drawImage(Main.Map.Tiles[x][y].Texture, x * 16, y * 16,
							null);
				}
			}
		} else {
			if (Main.frameb3 == 0) {
				for (int y = 0; y < 128; y++) {
					for (int x = 0; x < 512; x++) {
						if ((x + Main.render.xOffSet / 16 >= 0 && y
								+ Main.render.yOffSet / 16 >= 0)
								&& (x + Main.render.xOffSet / 16 <= 511 && y
										+ Main.render.yOffSet / 16 <= 511))
							g.drawImage(
									Main.Map.Tiles[x + Main.render.xOffSet / 16][y
											+ Main.render.yOffSet / 16].Texture,
									(x + Main.render.xOffSet / 16) * 16,
									(y + Main.render.yOffSet / 16) * 16, null);

					}
				}
			}
			if (Main.frameb3 == 1) {
				for (int y = 128; y < 256; y++) {
					for (int x = 0; x < 512; x++) {
						if ((x + Main.render.xOffSet / 16 >= 0 && y
								+ Main.render.yOffSet / 16 >= 0)
								&& (x + Main.render.xOffSet / 16 <= 511 && y
										+ Main.render.yOffSet / 16 <= 511))
							g.drawImage(
									Main.Map.Tiles[x + Main.render.xOffSet / 16][y
											+ Main.render.yOffSet / 16].Texture,
									(x + Main.render.xOffSet / 16) * 16,
									(y + Main.render.yOffSet / 16) * 16, null);

					}
				}
			}
			if (Main.frameb3 == 2) {
				for (int y = 256; y < 384; y++) {
					for (int x = 0; x < 512; x++) {
						if ((x + Main.render.xOffSet / 16 >= 0 && y
								+ Main.render.yOffSet / 16 >= 0)
								&& (x + Main.render.xOffSet / 16 <= 511 && y
										+ Main.render.yOffSet / 16 <= 511))
							g.drawImage(
									Main.Map.Tiles[x + Main.render.xOffSet / 16][y
											+ Main.render.yOffSet / 16].Texture,
									(x + Main.render.xOffSet / 16) * 16,
									(y + Main.render.yOffSet / 16) * 16, null);

					}
				}
			}
			if (Main.frameb3 == 3) {
				for (int y = 384; y < 512; y++) {
					for (int x = 0; x < 512; x++) {
						if ((x + Main.render.xOffSet / 16 >= 0 && y
								+ Main.render.yOffSet / 16 >= 0)
								&& (x + Main.render.xOffSet / 16 <= 511 && y
										+ Main.render.yOffSet / 16 <= 511))
							g.drawImage(
									Main.Map.Tiles[x + Main.render.xOffSet / 16][y
											+ Main.render.yOffSet / 16].Texture,
									(x + Main.render.xOffSet / 16) * 16,
									(y + Main.render.yOffSet / 16) * 16, null);

					}
				}
			}
		}
		g.drawImage(MapTile.road.Texture, (x * 16) + -Main.render.xOffSet,
				(y * 16) + -Main.render.yOffSet, null);

	}
}
