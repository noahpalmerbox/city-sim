package screen;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import resmgr.ImageLoader;

import main.Main;

public class HUD {
	public BufferedImage FPS;
	Graphics g;
	public void init(){
		g = Main.render.hud.getGraphics();
	
	}
	public void update(){
		g.drawImage(FPS, 8, 30, null);
		g.drawImage(ImageLoader.intParser(Main.fps), 80, 55, null);
		
	}

}
