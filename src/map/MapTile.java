package map;

import java.awt.image.BufferedImage;

import resmgr.ImageLoader;

public class MapTile {
	public static MapTile grass;
	public static MapTile road;
	public static MapTile ocean;
	public static MapTile sand;
	
	
	
	
	public static void init(){
		grass = new MapTile(ImageLoader.Textures[0], TileType.Fill);
		road = new MapTile(ImageLoader.Textures[1], TileType.Road);
		ocean = new MapTile(ImageLoader.Textures[2], TileType.Water);
		sand = new MapTile(ImageLoader.Textures[3], TileType.Fill);
		
		
		
	}
	public BufferedImage Texture;
	TileType type;
public MapTile(BufferedImage im, TileType tt){
	Texture = im;
	type = tt;
	}
}
